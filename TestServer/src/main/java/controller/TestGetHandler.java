
package controller;

import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by radheyshyam on 10/07/17.
 */
public class TestGetHandler implements Handler<RoutingContext> {
	private static final Logger logger = LoggerFactory.getLogger(TestGetHandler.class);

	@Override public void handle(RoutingContext routingContext) {

		routingContext.request().bodyHandler(body -> {
			System.out.println(body.toString());
			int randomNum = ThreadLocalRandom.current().nextInt(1, 50 + 1);
			try {
				Thread.sleep(randomNum);
			} catch (InterruptedException e) {
				logger.error(e.getMessage(),e);
			}
			getAllowedResponse(routingContext.request()).setStatusCode(200).end("SUCCESS SLOW("+randomNum+" ms)");

		});

	}
	private HttpServerResponse getAllowedResponse(HttpServerRequest req) {

		String origin = req.headers().get("origin");
		if (origin == null || "null".equals(origin)) {
			origin = "*";
		}
		System.out.println("Origin :" + origin);
		req.response().headers().set("Access-Control-Allow-Origin", origin);
		req.response().headers().set("Access-Control-Allow-Credentials", "true");
		String hdr = req.headers().get("Access-Control-Request-Headers");
		if (hdr != null) {
			req.response().headers().set("Access-Control-Allow-Headers", hdr);
		}
		return req.response();
	}
}
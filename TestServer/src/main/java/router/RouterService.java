package router;

import exception.VerticleException;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Created by radheyshyam on 10/07/17.
 */
public class RouterService {

	private static final long oneDaySeconds = 1 * 24 * 60 * 60;
	private static final long oneDayms = oneDaySeconds * 1000;
	private static final Logger logger = LoggerFactory.getLogger(RouterService.class);
	private final Router router;

	public RouterService(Vertx vertx) {

		this.router = Router.router(vertx);

		this.router.route().handler(CookieHandler.create());
		//router.route().handler(SessionHandler.create(ClusteredSessionStore.create(vertx)).setNagHttps(false));

		this.router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));

		this.router.route().handler(
				CorsHandler.create("*").allowedMethod(HttpMethod.GET).allowedMethod(HttpMethod.POST).allowedMethod(HttpMethod.PUT).allowedMethod(HttpMethod.OPTIONS)
						.allowCredentials(true).allowedHeader("Content-Type").allowedHeader("X-PINGARUNER"));

		this.router.route().consumes("application/json").produces("application/json");

		this.router.route().handler(ctx -> {
			setCORS(ctx);
			ctx.next();
		});

		this.router.route().failureHandler(ctx -> {
			logger.error(ctx.failure().getMessage());
			VerticleException exception = null;
			if (ctx.failure() instanceof VerticleException) {
				exception = (VerticleException) ctx.failure();
				final JsonObject error = new JsonObject().put("timestamp", System.nanoTime()).put("status", exception.getStatusCode())
						.put("error", HttpResponseStatus.valueOf(exception.getStatusCode()).reasonPhrase()).put("path", ctx.normalisedPath())
						.put("exception", exception.getClass().getName());

				if (exception.getMessage() != null) {
					error.put("message", exception.getMessage());
				}

				ctx.response().setStatusCode(exception.getStatusCode());
				ctx.response().end(error.encode());
			} else {
				final JsonObject error = new JsonObject().put("timestamp", System.nanoTime()).put("exception", ctx.failure().getClass().getName())
						.put("exceptionMessage", ctx.failure().getMessage()).put("path", ctx.request().path());

				ctx.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
				ctx.response().setStatusCode(500);
				ctx.response().end(error.encode());
			}

		});
	}

	public Router getRouter() {
		return router;
	}

	private void setCORS(RoutingContext rc) {

		String origin = rc.request().headers().get("origin");
		if (origin == null || "null".equals(origin)) {
			origin = "*";
		}
		String expires = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz").format(new Date(System.currentTimeMillis() + oneDayms));

		rc.response().putHeader("Expires", expires).putHeader("Access-Control-Max-Age", String.valueOf(oneDaySeconds))
				.putHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate")
				.putHeader("Pragma", "no-cache")
				.putHeader("Content-Type", "application/json; charset=utf-8")
				.putHeader("Access-Control-Allow-Origin", origin)
				.putHeader("Access-Control-Allow-Credentials", "true")
				// prevents Internet Explorer from MIME - sniffing a
				// response away from the declared content-type
				.putHeader("X-Content-Type-Options", "nosniff")
				// Strict HTTPS (for about ~6Months)
				.putHeader("Strict-Transport-Security", "max-age=" + 15768000)
				// IE8+ do not allow opening of attachments in the context of this resource
				.putHeader("X-Download-Options", "noopen")
				// enable XSS for IE
				.putHeader("X-XSS-Protection", "1; mode=block")
				// deny frames
				.putHeader("X-FRAME-OPTIONS", "DENY");
		String hdr = rc.request().headers().get("Access-Control-Request-Headers");
		if (hdr != null) {
			rc.response().putHeader("Access-Control-Allow-Headers", hdr);
		}
	}

}

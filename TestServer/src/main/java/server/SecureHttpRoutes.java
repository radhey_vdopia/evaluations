package server;

import controller.TestGetHandler;
import controller.TestPostHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import router.RouterService;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by radheyshyam on 10/07/17.
 */
public class SecureHttpRoutes extends AbstractVerticle {

	private static final Logger logger = LoggerFactory.getLogger(SecureHttpRoutes.class);
	private static final long oneDaySeconds = 1 * 24 * 60 * 60;
	private static final long oneDayms = oneDaySeconds * 1000;
	private static final long BLOCK_THREAD_CHECK_INTERVAL = 1000 * 60 * 60 * 3;
	private static final int cores = Runtime.getRuntime().availableProcessors();
	private static final int coreFactor = 1;
	private static final int KB = 1024;
	private static final int MB = 1024 * KB;


	public static void main(String[] args) {

		VertxOptions options = new VertxOptions().setClustered(false).setBlockedThreadCheckInterval(BLOCK_THREAD_CHECK_INTERVAL).setClustered(false);
		DeploymentOptions deploymentOptions = new DeploymentOptions().setWorker(true).setMultiThreaded(true);
		Runner.runExample(SecureHttpRoutes.class, options, null);
	}

	@Override public void start() throws Exception {

		Router router = (new RouterService(vertx)).getRouter();

		router.route("/").handler(routingContext -> {
			HttpServerResponse response = routingContext.response();
			response.putHeader("content-type", "text/html").end("<h1>Hello from my first Vert.x 3 application</h1>");
		});

		router.get("/testServer/get").handler(this::getData);
		router.get("/testServer/getSlow").handler(new TestGetHandler());
		router.get("/testServer/getSlowest").handler(this::slowest);
		router.post("/testServer/setData").handler(new TestPostHandler());

		router.route().last().handler(routingContext -> {
			routingContext.response().setStatusCode(404).end();
		});
		router.route().handler(BodyHandler.create().setBodyLimit(5 * MB));

		vertx.createHttpServer().requestHandler(router::accept).listen(9090);
	}

	private void getData(RoutingContext routingContext) {

		routingContext.response().setStatusCode(200).end("SUCCESS-FAST");
	}
	private void slowest(RoutingContext routingContext) {
		int randomNum = ThreadLocalRandom.current().nextInt(10, 400 + 1);
		try {
			Thread.sleep(randomNum);
		} catch (InterruptedException e) {
			logger.error(e.getMessage(),e);
		}
		routingContext.response().setStatusCode(200).end("SUCCESS-SLOWEST("+randomNum+" ms)");
	}


}

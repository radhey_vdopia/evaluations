package exception;

/**
 * Created by radheyshyam on 10/07/17.
 */
public class VerticleException  extends RuntimeException {

    private int statusCode;

    public VerticleException() {
        super();
        statusCode = 500;
    }

    public VerticleException(int code) {
        super();
        statusCode = code;
    }

    public VerticleException(String message) {
        super(message);
        statusCode = 500;
    }

    public VerticleException(Throwable cause) {
        super(cause);
        statusCode = 500;
    }

    public VerticleException(String message, Throwable cause) {
        super(message, cause);
        statusCode = 500;
    }

    public VerticleException(String message, int code) {
        super(message);
        statusCode = code;
    }

    public VerticleException(Throwable cause, int code) {
        super(cause);
        statusCode = code;
    }

    public VerticleException(String message, Throwable cause, int code) {
        super(message, cause);
        statusCode = code;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public static VerticleException failure(Throwable throwable) {
        if (throwable instanceof VerticleException) {
            return (VerticleException) throwable;
        }
        if (throwable.getMessage() == null) {
            return new VerticleException("No message provided", throwable.getCause());
        }
        return new VerticleException(throwable.getMessage(), throwable.getCause());
    }
}
package com.vdopia.http;

import com.vdopia.http.async.HttpRequestSender;
import com.vdopia.http.async.callback.HttpResponseCallbackHandlerIntf;
import com.vdopia.http.async.config.HttpConnectionCacheManager;
import com.vdopia.http.async.data.HttpAsyncResponse;
import com.vdopia.http.async.data.HttpRequest;
import com.vdopia.http.async.enums.HttpRequestType;
import com.vdopia.http.constant.SystemConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Created by radheyshyam on 10/07/17.
 */
public class ChocolateAsyncClientTest {
	
	public static void main(String[] args) {
		//For loading all configured connection
		HttpConnectionCacheManager connectionCacheManager = new HttpConnectionCacheManager();
		try {
			connectionCacheManager.loadHttpAsyncConnections();
		} catch (Exception e) {
			e.printStackTrace();
		}
		HttpRequest httpRequest = new HttpRequest(SystemConstants.ASYNC_CLIENT_BIDDER_1, HttpRequestType.GET, "http://www.rediff.com", null, null, null, new DummyHttpResponseCallbackHandlerImpl());
		HttpRequestSender httpRequestSender = new HttpRequestSender();
		httpRequestSender.call(httpRequest);
	}
	
	private static class DummyHttpResponseCallbackHandlerImpl implements HttpResponseCallbackHandlerIntf{

		private static Logger logger = LoggerFactory.getLogger(DummyHttpResponseCallbackHandlerImpl.class);

		public void execute(HttpAsyncResponse httpAsyncResponse) {
			try {
				System.out.println(httpAsyncResponse.getResponse());
				logger.error(httpAsyncResponse.getResponse());

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}

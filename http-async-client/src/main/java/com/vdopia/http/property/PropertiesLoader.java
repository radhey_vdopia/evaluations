
package com.vdopia.http.property;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;
/**
 * Created by radheyshyam on 10/07/17.
 */
public class PropertiesLoader {

	private static final Logger logger = LoggerFactory.getLogger(PropertiesLoader.class);

	private final Properties httpClientProperties;

	private PropertiesLoader() {
			httpClientProperties = loadProperties("http-async-client.properties");
	}

	private static final class PropertiesHolder {
		private static final PropertiesLoader INSTANCE;

		static {
			PropertiesLoader propertiesLoader = null;

			try {
				propertiesLoader = new PropertiesLoader();
			} catch (Throwable t) {
				throw new ExceptionInInitializerError(t);
			}

			INSTANCE = propertiesLoader;
		}
	}


	public static Properties loadProperties(String propertyFileName) {

		final Properties property = new Properties();

		final InputStream inputStream = PropertiesLoader.class.getClassLoader().getResourceAsStream(propertyFileName);

		if (inputStream == null) {
			logger.error("Error while loading property file as a stream.");
		}

		try {
			property.load(inputStream);
		} catch (Throwable t) {
			logger.error("Error while loading property file, named " + propertyFileName + ", cause : " + t.getMessage(), t);
		}
		return property;
	}

	public static Properties loadPropertiesFromPath(String propertyFileName)  {

		final Properties property = new Properties();

		FileInputStream inputStream = null;
		try {
			inputStream = new FileInputStream(propertyFileName);
		} catch (FileNotFoundException e) {
			logger.error("Error while loading property file as a stream.");
		}

		try {
			property.load(inputStream);
		} catch (Throwable t) {
			logger.error("Error while loading property file, named " + propertyFileName + ", cause : " + t.getMessage(), t);
		}

		return property;
	}

	public static final PropertiesLoader getInstance() {
		return PropertiesHolder.INSTANCE;
	}

	public static Properties getHttpClientProperties() {
		return (Properties) getInstance().httpClientProperties;
	}
}

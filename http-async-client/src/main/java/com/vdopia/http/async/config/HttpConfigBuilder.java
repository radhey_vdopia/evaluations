
package com.vdopia.http.async.config;

public class HttpConfigBuilder {

	private String configName;
	private boolean isCompressionEnabled = false;
	private boolean allowFollowRedirects = false;
	private int connectionTimeoutInMS = 60000;
	private int idleConnectionInPoolTimeoutInMs = 60000;
	private int requestTimeoutInMS = 60000;
	private int maximumConnectionsPerHost = 50;
	private int maximumConnectionsTotal = 50;
	private int readTimeoutInMs = 60000;
	private int threadPoolSize = 500;
	private int maxTTL = 60000;
	private int nioThreadCount = 10;
	private String userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1";
	private boolean isKeepAlive = false;

	public HttpConfigBuilder(String configName) {
		super();
		this.configName = configName;
	}


	public HttpConfigBuilder(String configName, boolean isCompressionEnabled, boolean allowFollowRedirects, int connectionTimeoutInMS,
							 int idleConnectionInPoolTimeoutInMs, int requestTimeoutInMS, int maximumConnectionsPerHost, int maximumConnectionsTotal, int readTimeoutInMs, int maxTTL, int nioThreadCount, boolean keepAliveFlag, String userAgent) {
		this.configName = configName;
		this.isCompressionEnabled = isCompressionEnabled;
		this.allowFollowRedirects = allowFollowRedirects;
		this.connectionTimeoutInMS = connectionTimeoutInMS;
		this.idleConnectionInPoolTimeoutInMs = idleConnectionInPoolTimeoutInMs;
		this.requestTimeoutInMS = requestTimeoutInMS;
		this.maximumConnectionsPerHost = maximumConnectionsPerHost;
		this.maximumConnectionsTotal = maximumConnectionsTotal;
		this.readTimeoutInMs = readTimeoutInMs;
		this.maxTTL = maxTTL;
		this.nioThreadCount = nioThreadCount;
		this.userAgent = userAgent;
		this.isKeepAlive = keepAliveFlag;
	}
	public HttpConfigBuilder(String configName, boolean isCompressionEnabled, boolean allowFollowRedirects, int connectionTimeoutInMS,
			int idleConnectionInPoolTimeoutInMs, int requestTimeoutInMS, int maximumConnectionsPerHost, int maximumConnectionsTotal, int readTimeoutInMs, int maxTTL, int nioThreadCount,boolean keepAliveFlag) {
		this.configName = configName;
		this.isCompressionEnabled = isCompressionEnabled;
		this.allowFollowRedirects = allowFollowRedirects;
		this.connectionTimeoutInMS = connectionTimeoutInMS;
		this.idleConnectionInPoolTimeoutInMs = idleConnectionInPoolTimeoutInMs;
		this.requestTimeoutInMS = requestTimeoutInMS;
		this.maximumConnectionsPerHost = maximumConnectionsPerHost;
		this.maximumConnectionsTotal = maximumConnectionsTotal;
		this.readTimeoutInMs = readTimeoutInMs;
		this.maxTTL = maxTTL;
		this.nioThreadCount = nioThreadCount;
		this.isKeepAlive = keepAliveFlag;
	}

	public String getConfigName() {
		return configName;
	}

	public boolean isCompressionEnabled() {
		return isCompressionEnabled;
	}

	public boolean isAllowFollowRedirects() {
		return allowFollowRedirects;
	}


	public int getConnectionTimeoutInMS() {
		return connectionTimeoutInMS;
	}

	public int getIdleConnectionInPoolTimeoutInMs() {
		return idleConnectionInPoolTimeoutInMs;
	}

	public int getRequestTimeoutInMS() {
		return requestTimeoutInMS;
	}

	public int getMaximumConnectionsPerHost() {
		return maximumConnectionsPerHost;
	}

	public int getMaximumConnectionsTotal() {
		return maximumConnectionsTotal;
	}

	public int getReadTimeoutInMs() {
		return readTimeoutInMs;
	}

	public int getThreadPoolSize() {
		return threadPoolSize;
	}

	public int getMaxTTL() {
		return maxTTL;
	}

	public void setMaxTTL(int maxTTL) {
		this.maxTTL = maxTTL;
	}

	public int getNioThreadCount() {
		return nioThreadCount;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public boolean isKeepAlive() {
		return isKeepAlive;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (allowFollowRedirects ? 1231 : 1237);
		result = prime * result + ((configName == null) ? 0 : configName.hashCode());
		result = prime * result + connectionTimeoutInMS;
		result = prime * result + idleConnectionInPoolTimeoutInMs;
		result = prime * result + (isCompressionEnabled ? 1231 : 1237);
		result = prime * result + maximumConnectionsPerHost;
		result = prime * result + maximumConnectionsTotal;
		result = prime * result + readTimeoutInMs;
		result = prime * result + requestTimeoutInMS;
		result = prime * result + threadPoolSize;
		result = prime * result + nioThreadCount;
		result = prime * result + maxTTL;
		result = prime * result + (isKeepAlive ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HttpConfigBuilder other = (HttpConfigBuilder) obj;
		if (allowFollowRedirects != other.allowFollowRedirects)
			return false;
		if (isKeepAlive != other.isKeepAlive)
			return false;
		if (configName == null) {
			if (other.configName != null)
				return false;
		} else if (!configName.equals(other.configName))
			return false;
		if (connectionTimeoutInMS != other.connectionTimeoutInMS)
			return false;
		if (idleConnectionInPoolTimeoutInMs != other.idleConnectionInPoolTimeoutInMs)
			return false;
		if (isCompressionEnabled != other.isCompressionEnabled)
			return false;
		if (maximumConnectionsPerHost != other.maximumConnectionsPerHost)
			return false;
		if (maximumConnectionsTotal != other.maximumConnectionsTotal)
			return false;
		if (readTimeoutInMs != other.readTimeoutInMs)
			return false;
		if (requestTimeoutInMS != other.requestTimeoutInMS)
			return false;
		if (maxTTL != other.maxTTL)
			return false;
		if (threadPoolSize != other.threadPoolSize)
			return false;
		if (nioThreadCount != other.nioThreadCount)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HttpConfigBuilder [configName=" + configName + ", isCompressionEnabled=" + isCompressionEnabled
				+ ", allowFollowRedirects=" + allowFollowRedirects  + ", connectionTimeoutInMS=" + connectionTimeoutInMS
				+ ", idleConnectionInPoolTimeoutInMs=" + idleConnectionInPoolTimeoutInMs + ", requestTimeoutInMS="
				+ requestTimeoutInMS + ", maximumConnectionsPerHost=" + maximumConnectionsPerHost
				+ ", maximumConnectionsTotal=" + maximumConnectionsTotal + ", readTimeoutInMs=" + readTimeoutInMs
				+ ", threadPoolSize=" + threadPoolSize + ", maxTTL=" + maxTTL + ", userAgent=" + userAgent + "]";
	}

}

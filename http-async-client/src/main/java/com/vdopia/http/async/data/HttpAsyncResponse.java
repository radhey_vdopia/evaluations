
package com.vdopia.http.async.data;

/**
 * Created by radheyshyam on 10/07/17.
 */
import io.netty.handler.codec.http.HttpHeaders;

public class HttpAsyncResponse {

	public enum HTTP_REQUEST_STATUS {
		SUCCESS, FAILURE, FAILURE_RESQUEST_TIMEOUT, FAILURE_READ_TIMEOUT
	}

	private String requestGuid;
	private HTTP_REQUEST_STATUS status;
	private Integer statusCode;
	private String response;
	private HttpHeaders headers;

	public HttpAsyncResponse() {
		this.requestGuid = new com.eaio.uuid.UUID().toString();
	}

	public String getRequestGuid() {
		return requestGuid;
	}

	public HTTP_REQUEST_STATUS getStatus() {
		return status;
	}

	public void setStatus(HTTP_REQUEST_STATUS status) {
		this.status = status;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public HttpHeaders getHeaders() {
		return headers;
	}

	public void setHeaders(HttpHeaders headers) {
		this.headers = headers;
	}

	@Override
	public String toString() {
		return "HttpAsyncResponse [requestGuid=" + requestGuid + ", status=" + status + ", statusCode=" + statusCode + ", response=" + response + ", headers=" + headers + "]";
	}

}


package com.vdopia.http.async.callback;

import com.vdopia.http.async.data.HttpAsyncResponse;
/**
 * Created by radheyshyam on 10/07/17.
 */
public interface HttpResponseCallbackHandlerIntf {
	
	public void execute(HttpAsyncResponse httpAsyncResponse);

}
 
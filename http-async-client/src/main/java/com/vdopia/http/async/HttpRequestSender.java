
package com.vdopia.http.async;

import com.vdopia.http.async.config.HttpClientConnectionPoolManager;
import com.vdopia.http.async.config.HttpConfigBuilder;
import com.vdopia.http.async.core.HttpCallDispatcher;
import com.vdopia.http.async.data.HttpRequest;
import org.asynchttpclient.AsyncHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Created by radheyshyam on 10/07/17.
 */
public class HttpRequestSender {

	private static final Logger logger = LoggerFactory.getLogger(HttpRequestSender.class);
	private final HttpClientConnectionPoolManager asyncClientPool = new HttpClientConnectionPoolManager();

	public void call(HttpRequest httpRequest) {

		try {

			AsyncHttpClient asyncHttpClient = asyncClientPool.fetchHttpClientFromCache(httpRequest.getConfigReference());
			if (null == asyncHttpClient) {
				logger.error("Http config reference value not found for "+httpRequest.getConfigReference());
				asyncHttpClient = asyncClientPool.buildAndFetchHttpClient(new HttpConfigBuilder("test"));
			}
			HttpCallDispatcher client = new HttpCallDispatcher(asyncHttpClient);

			switch (httpRequest.getHttpRequestType()) {
				case GET:
					client.buildGetCaller(httpRequest.getUrl(), httpRequest.getParams(), httpRequest.getHeaders(), httpRequest.getHttpAsyncResponsePostExecutionHook());
					break;
				case POST_PARAM_DATA:
					client.buildPostCallerForPostParameters(httpRequest.getUrl(), httpRequest.getParams(), httpRequest.getHeaders(),
							httpRequest.getHttpAsyncResponsePostExecutionHook());
					break;
				case POST_BODY_DATA:
					client.buildPostCallerForPostBody(httpRequest.getUrl(), httpRequest.getPostBody(), httpRequest.getHeaders(),
							httpRequest.getHttpAsyncResponsePostExecutionHook());
					break;
				default:
					client.buildGetCaller(httpRequest.getUrl(), httpRequest.getParams(), httpRequest.getHeaders(), httpRequest.getHttpAsyncResponsePostExecutionHook());
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

}

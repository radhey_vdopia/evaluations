
package com.vdopia.http.async.core;

import com.vdopia.http.async.callback.HttpResponseCallbackHandlerIntf;
import com.vdopia.http.async.callback.HttpResponseReader;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.BoundRequestBuilder;
import org.asynchttpclient.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
/**
 * Created by radheyshyam on 10/07/17.
 */
public class HttpCallDispatcher {

	private static final Logger logger = LoggerFactory.getLogger(HttpCallDispatcher.class);
	private static final String headerExceptionName = "Connection";

	private AsyncHttpClient asyncHttpClient;

	public HttpCallDispatcher(AsyncHttpClient asyncHttpClient) {
		this.asyncHttpClient = asyncHttpClient;
	}

	public void buildGetCaller(String url, Map<String, String> params, Map<String, String> headers, HttpResponseCallbackHandlerIntf httpAsyncResponsePostExecutionHook) {

		BoundRequestBuilder boundRequestBuilder = asyncHttpClient.prepareGet(url);

		if (headers != null && headers.size() > 0) {
			Set<Entry<String, String>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, String>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null && !entry.getKey().equalsIgnoreCase(headerExceptionName)) {
					final String headerKey = entry.getKey();
					final String headerValue = entry.getValue();
					boundRequestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (params != null && params.size() > 0) {
			Set<Entry<String, String>> postParamsEntrySet = params.entrySet();

			for (Iterator<Entry<String, String>> iterator = postParamsEntrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null) {
					final String paramKey = entry.getKey();
					final String paramValue = entry.getValue();
					boundRequestBuilder.addQueryParam(paramKey, paramValue);
				}
			}

		}

		try {
			Request request = boundRequestBuilder.build();

			HttpResponseReader httpAsyncResponseReader = new HttpResponseReader(httpAsyncResponsePostExecutionHook);
			asyncHttpClient.executeRequest(request, httpAsyncResponseReader);

		} catch (Exception e) {
			logger.error("Error in doing GET for URL=> " + url, e);

		} finally {}

	}

	public void buildPostCallerForPostParameters(String url, Map<String, String> params, Map<String, String> headers,
			HttpResponseCallbackHandlerIntf httpAsyncResponsePostExecutionHook) {

		BoundRequestBuilder requestBuilder = asyncHttpClient.preparePost(url);

		if (headers != null && headers.size() > 0) {
			Set<Entry<String, String>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, String>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null && !entry.getKey().equalsIgnoreCase(headerExceptionName)) {
					final String headerKey = entry.getKey();
					final String headerValue = entry.getValue();
					requestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (params != null && params.size() > 0) {
			Set<Entry<String, String>> postParamsEntrySet = params.entrySet();
			for (Iterator<Entry<String, String>> iterator = postParamsEntrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null) {
					final String paramKey = entry.getKey();
					final String paramValue = entry.getValue();
					requestBuilder.addFormParam(paramKey, paramValue);
				}
			}

		}

		try {
			Request request = requestBuilder.build();
			
			logger.info("{Post for Parameters} url=> " + url + ", parameters=> " + params);

			HttpResponseReader httpAsyncResponseReader = new HttpResponseReader(httpAsyncResponsePostExecutionHook);
			asyncHttpClient.executeRequest(request, httpAsyncResponseReader);

		} catch (Exception e) {
			logger.error("Exception doing POST for URL: " + url, e);
		} finally {}

	}

	public void buildPostCallerForPostBody(String url, String postBody, Map<String, String> headers, HttpResponseCallbackHandlerIntf httpAsyncResponsePostExecutionHook)
			throws IllegalArgumentException, UnsupportedEncodingException {

		BoundRequestBuilder requestBuilder = asyncHttpClient.preparePost(url);

		if (headers != null && headers.size() > 0) {
			Set<Entry<String, String>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, String>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null && !entry.getKey().equalsIgnoreCase(headerExceptionName)) {
					final String headerKey = entry.getKey();
					final String headerValue = entry.getValue();
					requestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (postBody != null) {
			requestBuilder.setBody(postBody.getBytes("UTF-8"));
		}

		try {
			Request request = requestBuilder.build();
			logger.info("Post call with body url=> " + url + ", body=> " + postBody);

			HttpResponseReader httpAsyncResponseReader = new HttpResponseReader(httpAsyncResponsePostExecutionHook);
			asyncHttpClient.executeRequest(request, httpAsyncResponseReader);

		} catch (Exception e) {
			logger.error("Exception doing POST for URL=> " + url, e);
		} finally {}

	}

}

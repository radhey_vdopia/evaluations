
package com.vdopia.http.async.enums;
/**
 * Created by radheyshyam on 10/07/17.
 */
public enum HttpRequestType {
	GET, POST_PARAM_DATA, POST_BODY_DATA
}

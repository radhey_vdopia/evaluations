package com.vdopia.http.async.data;

import com.vdopia.http.async.callback.HttpResponseCallbackHandlerIntf;
import com.vdopia.http.async.enums.HttpRequestType;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by radheyshyam on 10/07/17.
 */
public class HttpRequest implements Serializable {

	private static final long serialVersionUID = -2929125017641658735L;
	private final String configReference;
	private final HttpRequestType httpRequestType;
	private final String url;
	private final Map<String, String> params;
	private final Map<String, String> headers;
	private final String postBody;
	private final HttpResponseCallbackHandlerIntf httpAsyncResponsePostExecutionHook;

	public HttpRequest(String configReference, HttpRequestType httpRequestType, String url, Map<String, String> params,
			Map<String, String> headers, String postBody,
			HttpResponseCallbackHandlerIntf httpAsyncResponsePostExecutionHook) {
		this.configReference = configReference;
		this.httpRequestType = httpRequestType;
		this.url = url;
		this.params = params;
		this.headers = headers;
		this.postBody = postBody;
		this.httpAsyncResponsePostExecutionHook = httpAsyncResponsePostExecutionHook;
	}

	public String getConfigReference() {
		return configReference;
	}

	public HttpRequestType getHttpRequestType() {
		return httpRequestType;
	}

	public String getUrl() {
		return url;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public String getPostBody() {
		return postBody;
	}

	public HttpResponseCallbackHandlerIntf getHttpAsyncResponsePostExecutionHook() {
		return httpAsyncResponsePostExecutionHook;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((configReference == null) ? 0 : configReference.hashCode());
		result = prime * result + ((headers == null) ? 0 : headers.hashCode());
		result = prime * result
				+ ((httpAsyncResponsePostExecutionHook == null) ? 0 : httpAsyncResponsePostExecutionHook.hashCode());
		result = prime * result + ((httpRequestType == null) ? 0 : httpRequestType.hashCode());
		result = prime * result + ((params == null) ? 0 : params.hashCode());
		result = prime * result + ((postBody == null) ? 0 : postBody.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HttpRequest other = (HttpRequest) obj;
		if (configReference == null) {
			if (other.configReference != null)
				return false;
		} else if (!configReference.equals(other.configReference))
			return false;
		if (headers == null) {
			if (other.headers != null)
				return false;
		} else if (!headers.equals(other.headers))
			return false;
		if (httpAsyncResponsePostExecutionHook == null) {
			if (other.httpAsyncResponsePostExecutionHook != null)
				return false;
		} else if (!httpAsyncResponsePostExecutionHook.equals(other.httpAsyncResponsePostExecutionHook))
			return false;
		if (httpRequestType != other.httpRequestType)
			return false;
		if (params == null) {
			if (other.params != null)
				return false;
		} else if (!params.equals(other.params))
			return false;
		if (postBody == null) {
			if (other.postBody != null)
				return false;
		} else if (!postBody.equals(other.postBody))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HttpRequest [configReference=" + configReference + ", httpRequestType=" + httpRequestType + ", url="
				+ url + ", params=" + params + ", headers=" + headers + ", postBody=" + postBody
				+ ", httpAsyncResponsePostExecutionHook=" + httpAsyncResponsePostExecutionHook + "]";
	}

}

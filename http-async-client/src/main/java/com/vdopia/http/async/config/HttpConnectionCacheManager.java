
package com.vdopia.http.async.config;

import com.vdopia.http.constant.SystemConstants;
import com.vdopia.http.property.PropertiesLoader;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by radheyshyam on 10/07/17.
 */

public class HttpConnectionCacheManager {
	private static final Logger logger = LoggerFactory.getLogger(HttpConnectionCacheManager.class);
	private static HttpClientConnectionPoolManager asyncClientPool = new HttpClientConnectionPoolManager();

	public void loadHttpAsyncConnections() throws Exception {

		HttpConfigBuilder asysncConfig = setConfigParams();
		asyncClientPool.buildAndFetchHttpClient(asysncConfig);

		asysncConfig = new HttpConfigBuilder(SystemConstants.ASYNC_CLIENT_BIDDER_1, false, false, 180000, 180000, 180000,
				5000, 50000, 180000, 180000,20,false, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1");
		asyncClientPool.buildAndFetchHttpClient(asysncConfig);
		//with keepAlive setting true and high ttl
		asysncConfig = new HttpConfigBuilder(SystemConstants.ASYNC_CLIENT_BIDDER_2, false, false, 350, 10000, 300, 100, 250, 300,10000,10,true);
		asyncClientPool.buildAndFetchHttpClient(asysncConfig);

		asysncConfig = new HttpConfigBuilder(SystemConstants.ASYNC_CLIENT_BIDDER_3, false, false, 600, 1000, 600,
				100, 200, 500, 500,20, false,"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1");
		asyncClientPool.buildAndFetchHttpClient(asysncConfig);
	}


	public static HttpConfigBuilder setConfigParams() {
		HttpConfigBuilder clientConfigBuilder = null;
		 boolean isCompressionEnabled = StringUtils.contains(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.compression.enable"), "true") ? true : false;
		 boolean allowFollowRedirects = StringUtils.contains(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.followRedirects.enable"), "true") ? true
				 : false;
		 int connectionTimeoutInMS = Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.connection.timeout"));
		 int idleConnectionInPoolTimeoutInMs = Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty(
				 "http.connector.idealConnection.timeout"));
		 int requestTimeoutInMS = Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.request.timeout"));
		 int maximumConnectionsPerHost = Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.connection.perhost"));
		 int maximumConnectionsTotal = Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.connection.total"));
		 int readTimeoutInMs = Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.read.timeout"));
		 int threadPoolSize = Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.thread.pool.size"));
		 int maxTTL = Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.max.ttl"));
		 int nioThreadCount = Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.max.ttl"));
		 String userAgent = PropertiesLoader.getHttpClientProperties().getProperty("http.connector.user.agent");
		 boolean isKeepAlive = StringUtils.contains(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.keepalive"), "true") ? true : false;;

		try {
			clientConfigBuilder = new HttpConfigBuilder(SystemConstants.ASYNC_CLIENT_DEFAULT,isCompressionEnabled,allowFollowRedirects, connectionTimeoutInMS, idleConnectionInPoolTimeoutInMs, requestTimeoutInMS,
					maximumConnectionsPerHost, maximumConnectionsTotal, readTimeoutInMs,maxTTL, nioThreadCount, isKeepAlive, userAgent);
		} catch (NumberFormatException ne) {
			logger.error("ning configuration file is not in proper format " + ne.getMessage(), ne);
		}

		return clientConfigBuilder;
	}


}

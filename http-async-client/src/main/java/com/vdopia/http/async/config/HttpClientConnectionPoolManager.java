
package com.vdopia.http.async.config;

import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.HashedWheelTimer;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.AsyncHttpClientConfig;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClientConfig;
import org.asynchttpclient.channel.ChannelPool;
import org.asynchttpclient.handler.resumable.ResumableIOExceptionFilter;
import org.asynchttpclient.netty.channel.DefaultChannelPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
/**
 * Created by radheyshyam on 10/07/17.
 */
public class HttpClientConnectionPoolManager {

	private static final Logger logger = LoggerFactory.getLogger(HttpClientConnectionPoolManager.class);
	private static Map<String, AsyncHttpClient> httpClientMap = new ConcurrentHashMap<String, AsyncHttpClient>();

	public AsyncHttpClient buildAndFetchHttpClient(HttpConfigBuilder clientConfig) {

		AsyncHttpClient asyncHttpClient = null;

		try {

			if (httpClientMap.containsKey(clientConfig.getConfigName())) {
				asyncHttpClient = httpClientMap.get(clientConfig.getConfigName());
			} else {
				asyncHttpClient = createAsyncHttpClient(clientConfig);
				httpClientMap.put(clientConfig.getConfigName(), asyncHttpClient);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return asyncHttpClient;

	}

	public AsyncHttpClient fetchHttpClientFromCache(String httpClientIdentifier) {

		AsyncHttpClient httpClient = null;

		try {

			if (httpClientMap.containsKey(httpClientIdentifier)) {
				httpClient = httpClientMap.get(httpClientIdentifier);
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return httpClient;

	}

	private AsyncHttpClient createAsyncHttpClient(HttpConfigBuilder clientConfig) {
		AsyncHttpClient asyncHttpClient = null;

		try {
//			HashedWheelTimer timer = new HashedWheelTimer();
//			timer.start();
//			ChannelPool pool =
//					new DefaultChannelPool(10000, -1, DefaultChannelPool.PoolLeaseStrategy.LIFO, timer,15000);

			final AsyncHttpClientConfig configBuilder = new DefaultAsyncHttpClientConfig.Builder()
					.setCompressionEnforced(clientConfig.isCompressionEnabled())
					.setFollowRedirect(clientConfig.isAllowFollowRedirects())
					.setConnectTimeout(clientConfig.getConnectionTimeoutInMS())
					.setPooledConnectionIdleTimeout(clientConfig.getIdleConnectionInPoolTimeoutInMs())
					.setRequestTimeout(clientConfig.getRequestTimeoutInMS())
					.setMaxConnectionsPerHost(clientConfig.getMaximumConnectionsPerHost())
					.setMaxConnections(clientConfig.getMaximumConnectionsTotal())
					.setKeepAlive(clientConfig.isKeepAlive())
					.setReadTimeout(clientConfig.getReadTimeoutInMs()).setConnectionTtl(clientConfig.getMaxTTL())
					.setEventLoopGroup( new NioEventLoopGroup(clientConfig.getNioThreadCount()))
//					.setChannelPool(pool)
					.addIOExceptionFilter(new ResumableIOExceptionFilter()).build();

			asyncHttpClient = new DefaultAsyncHttpClient((configBuilder));

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return asyncHttpClient;
	}

	public boolean shutDownAsyncHttpClient(String httpClientIdentifier) {

		boolean status = false;
		AsyncHttpClient httpClient = null;

		try {

			if (httpClientMap.containsKey(httpClientIdentifier)) {

				httpClient = httpClientMap.remove(httpClientIdentifier);
				httpClient.close();

			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return status;

	}

	public void shutDown() {

		try {

			logger.error("Shutting down HTTP Callers");
			System.out.println("Shutting down HTTP Callers");

			Set<Entry<String, AsyncHttpClient>> httpClientEntrySet = new HashMap<String, AsyncHttpClient>(
					httpClientMap).entrySet();

			for (Entry<String, AsyncHttpClient> entry : httpClientEntrySet) {

				if (entry != null) {
					String key = entry.getKey();
					shutDownAsyncHttpClient(key);
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

}


package com.vdopia.http.async.callback;

import com.vdopia.http.async.data.HttpAsyncResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.timeout.ReadTimeoutException;
import org.asynchttpclient.AsyncHandler;
import org.asynchttpclient.HttpResponseBodyPart;
import org.asynchttpclient.HttpResponseHeaders;
import org.asynchttpclient.HttpResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeoutException;
/**
 * Created by radheyshyam on 10/07/17.
 */
public class HttpResponseReader implements AsyncHandler<HttpAsyncResponse> {
	private static final Logger logger = LoggerFactory.getLogger(HttpResponseReader.class);
	private final StringBuilder responseAccumulator = new StringBuilder();
	private boolean isFailure = false;
	private boolean isPostExecutionHookCalled = false;
	private static final int HTTP_STATUS_OK = 200;

	private final HttpAsyncResponse httpResponse = new HttpAsyncResponse();
	private final HttpResponseCallbackHandlerIntf responseCallbackHandler;

	public HttpResponseReader(HttpResponseCallbackHandlerIntf responseCallbackHandler) {
		this.responseCallbackHandler = responseCallbackHandler;
	}


	public void onThrowable(Throwable t) {

		isFailure = true;

		if (t instanceof TimeoutException) {

			this.httpResponse.setStatus(HttpAsyncResponse.HTTP_REQUEST_STATUS.FAILURE_RESQUEST_TIMEOUT);

		} else if (t instanceof ReadTimeoutException) {

			this.httpResponse.setStatus(HttpAsyncResponse.HTTP_REQUEST_STATUS.FAILURE_READ_TIMEOUT);

		} else {

			this.httpResponse.setStatus(HttpAsyncResponse.HTTP_REQUEST_STATUS.FAILURE);

		}
		logger.error("Error {} ", t.getMessage());
		executionCompletionTrigger("onThrowable");
	}

	public State onBodyPartReceived(HttpResponseBodyPart httpResponseBodyPart) throws Exception {

		if (!isFailure) {
			responseAccumulator.append(new String(httpResponseBodyPart.getBodyPartBytes()));
		}
		return State.CONTINUE;

	}

	public State onStatusReceived(HttpResponseStatus httpResponseStatus) throws Exception {
		try {
			isFailure = (httpResponseStatus.getStatusCode() != HTTP_STATUS_OK);
			this.httpResponse.setStatusCode(httpResponseStatus.getStatusCode());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return isFailure ? State.ABORT : State.CONTINUE;
	}

	public State onHeadersReceived(HttpResponseHeaders httpHeaders) throws Exception {
		try {
			HttpHeaders headers = httpHeaders.getHeaders();
			this.httpResponse.setHeaders(headers);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return State.CONTINUE;

	}

	public HttpAsyncResponse onCompleted() throws Exception {
		try {
			String response = responseAccumulator.toString();
			this.httpResponse.setResponse(response);
			this.httpResponse.setStatus(HttpAsyncResponse.HTTP_REQUEST_STATUS.SUCCESS);

		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			executionCompletionTrigger("onCompleted");
		}
		return this.httpResponse;
	}

	private void executionCompletionTrigger(String responseStatus) {
		try {
			if (!isPostExecutionHookCalled) {

				isPostExecutionHookCalled = true;
				logger.info("Response status => {} for response data=> {} ", responseStatus, httpResponse.toString());
				if (this.responseCallbackHandler != null) {
					this.responseCallbackHandler.execute(this.httpResponse);
				}
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

}


package com.vdopia.http.sync;

import com.vdopia.http.property.PropertiesLoader;
import org.apache.commons.lang3.StringUtils;
import org.asynchttpclient.*;
import org.asynchttpclient.handler.resumable.ResumableIOExceptionFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
/**
 * Created by radheyshyam on 10/07/17.
 */
public class AsyncDefaultHttpClient {
	private static final Logger logger = LoggerFactory.getLogger(AsyncDefaultHttpClient.class);
	private static final ClientConfigBuilder CLIENT_CONFIG_BUILDER;
	private static final DefaultAsyncHttpClientConfig.Builder configBuilder;

	static {
		CLIENT_CONFIG_BUILDER = setConfigParams();
		configBuilder = asyncHttpClentConfigBuilder();
		if (logger.isInfoEnabled()) {
			logger.info("--AsyncNingHttp Client Pool loaded--");
		}
	}

	private static ClientConfigBuilder setConfigParams() {
		ClientConfigBuilder clientConfigBuilder = new ClientConfigBuilder();

		clientConfigBuilder
				.setCompressionEnabled(StringUtils.contains(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.compression.enable"), "true") ? true : false);
		clientConfigBuilder
				.setFollowRedirects(StringUtils.contains(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.followRedirects.enable"), "true") ? true
						: false);
		
		try {
			clientConfigBuilder.setConnectionTimeoutInMs(Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.connection.timeout")));
			clientConfigBuilder.setIdleConnectionInPoolTimeoutInMs(Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty(
					"http.connector.idealConnection.timeout")));
			clientConfigBuilder.setRequestTimeoutInMs(Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.request.timeout")));
			clientConfigBuilder.setMaximumConnectionsPerHost(Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.connection.perhost")));
			clientConfigBuilder.setMaximumConnectionsTotal(Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.connection.total")));
			clientConfigBuilder.setReadTimeOut(Integer.parseInt(PropertiesLoader.getHttpClientProperties().getProperty("http.connector.read.timeout")));
		} catch (NumberFormatException ne) {
			logger.error("ning configuration file is not in proper format " + ne.getMessage(), ne);
		}

		return clientConfigBuilder;
	}

	private static DefaultAsyncHttpClientConfig.Builder asyncHttpClentConfigBuilder() {
		DefaultAsyncHttpClientConfig.Builder configBuilder = new DefaultAsyncHttpClientConfig.Builder().setCompressionEnforced(CLIENT_CONFIG_BUILDER.isCompressionEnabled())
				.setFollowRedirect(CLIENT_CONFIG_BUILDER.isFollowRedirects())
				.setConnectTimeout(CLIENT_CONFIG_BUILDER.getConnectionTimeoutInMs()).setPooledConnectionIdleTimeout(CLIENT_CONFIG_BUILDER.getIdleConnectionInPoolTimeoutInMs())
				.setRequestTimeout(CLIENT_CONFIG_BUILDER.getRequestTimeoutInMs()).setMaxConnectionsPerHost(CLIENT_CONFIG_BUILDER.getMaximumConnectionsPerHost())
				.setMaxConnections(CLIENT_CONFIG_BUILDER.getMaximumConnectionsTotal()).setReadTimeout(CLIENT_CONFIG_BUILDER.getReadTimeOut())
				.addIOExceptionFilter(new ResumableIOExceptionFilter());
		return configBuilder;
	}

	private static AsyncHttpClient asyncHttpClient = new DefaultAsyncHttpClient(configBuilder.build());

	public Response doGetUrlResponse(String url, Map<String, String> getParams, Map<String, String> headers) {

		Response response = null;
		BoundRequestBuilder boundRequestBuilder = asyncHttpClient.prepareGet(url);
		if (headers != null && headers.size() > 0) {
			Set<Entry<String, String>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, String>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null) {
					final String headerKey = entry.getKey();
					final String headerValue = entry.getValue();
					boundRequestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (getParams != null && getParams.size() > 0) {
			Set<Entry<String, String>> postParamsEntrySet = getParams.entrySet();

			for (Iterator<Entry<String, String>> iterator = postParamsEntrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null) {
					final String paramKey = entry.getKey();
					final String paramValue = entry.getValue();
					boundRequestBuilder.addFormParam(paramKey, paramValue);
				}
			}

		}

		try {
			Future<Response> responseFuture = boundRequestBuilder.execute();
			try {
				response = responseFuture.get();
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			} catch (ExecutionException e) {
				logger.error(e.getMessage(), e);
			}
		} catch (Exception e) {
			logger.error("Error in doing GET call for URL: " + url, e);

		}

		return response;

	}

	public Response doGetUrlResponseWithQueryParams(String url, Map<String, Object> getParams, Map<String, Object> headers) {

		Response response = null;
		BoundRequestBuilder boundRequestBuilder = asyncHttpClient.prepareGet(url);
		if (headers != null && headers.size() > 0) {
			Set<Entry<String, Object>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, Object>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, Object> entry = iterator.next();

				if (entry != null && null != entry.getKey()) {
					final String headerKey = entry.getKey();
					final String headerValue = null != entry.getValue() ? entry.getValue().toString() : "";
					boundRequestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (getParams != null && getParams.size() > 0) {
			Set<Entry<String, Object>> paramsEntrySet = getParams.entrySet();

			for (Iterator<Entry<String, Object>> iterator = paramsEntrySet.iterator(); iterator.hasNext();) {
				Entry<String, Object> entry = iterator.next();

				if (entry != null && null != entry.getKey()) {
					final String paramKey = entry.getKey();
					final String paramValue = null != entry.getValue() ? entry.getValue().toString() : "";
					boundRequestBuilder.addQueryParam(paramKey, paramValue);
				}
			}

		}

		try {
			Future<Response> responseFuture = boundRequestBuilder.execute();
			try {
				response = responseFuture.get();
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			} catch (ExecutionException e) {
				logger.error(e.getMessage(), e);
			}
		} catch (Exception e) {
			logger.error("Error in doing GET call for URL: " + url, e);

		}

		return response;

	}

	public Response doGetUrlParmasResponse(String url, Map<String, String> getParams, Map<String, String> headers) {

		Response response = null;
		BoundRequestBuilder boundRequestBuilder = asyncHttpClient.prepareGet(url);
		if (headers != null && headers.size() > 0) {
			Set<Entry<String, String>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, String>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null) {
					final String headerKey = entry.getKey();
					final String headerValue = entry.getValue();
					boundRequestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (getParams != null && getParams.size() > 0) {
			Set<Entry<String, String>> postParamsEntrySet = getParams.entrySet();

			for (Iterator<Entry<String, String>> iterator = postParamsEntrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null) {
					final String paramKey = entry.getKey();
					final String paramValue = entry.getValue();
					boundRequestBuilder.addQueryParam(paramKey, paramValue);
				}
			}

		}

		try {
			Future<Response> responseFuture = boundRequestBuilder.execute();
			try {
				response = responseFuture.get();
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			} catch (ExecutionException e) {
				logger.error(e.getMessage(), e);
			}
		} catch (Exception e) {
			logger.error("Error in doing GET call for URL: " + url, e);

		}

		return response;

	}

	public Response doPostUrlResponse(String url, Map<String, String> postParams, Map<String, String> headers) {

		Response response = null;

		BoundRequestBuilder boundRequestBuilder = asyncHttpClient.preparePost(url);

		if (headers != null && headers.size() > 0) {
			Set<Entry<String, String>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, String>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null) {
					final String headerKey = entry.getKey();
					final String headerValue = entry.getValue();
					boundRequestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (postParams != null && postParams.size() > 0) {
			Set<Entry<String, String>> postParamsEntrySet = postParams.entrySet();
			for (Iterator<Entry<String, String>> iterator = postParamsEntrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null) {
					final String paramKey = entry.getKey();
					final String paramValue = entry.getValue();
					boundRequestBuilder.addFormParam(paramKey, paramValue);
				}
			}

		}

		try {
			Future<Response> responseFuture = boundRequestBuilder.execute();
			try {
				response = responseFuture.get();
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			} catch (ExecutionException e) {
				logger.error(e.getMessage(), e);
			}
		} catch (Exception e) {
			logger.error("Error in doing POST call for URL: " + url, e);
		}

		return response;

	}

	public Response doPostUrlResponseNew(String url, Map<String, Object> postParams, Map<String, Object> headers) {

		Response response = null;

		BoundRequestBuilder boundRequestBuilder = asyncHttpClient.preparePost(url);

		if (headers != null && headers.size() > 0) {
			Set<Entry<String, Object>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, Object>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, Object> entry = iterator.next();

				if (entry != null) {
					final String headerKey = entry.getKey();
					final String headerValue = (String) entry.getValue();
					boundRequestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (postParams != null && postParams.size() > 0) {
			Set<Entry<String, Object>> postParamsEntrySet = postParams.entrySet();
			for (Iterator<Entry<String, Object>> iterator = postParamsEntrySet.iterator(); iterator.hasNext();) {
				Entry<String, Object> entry = iterator.next();

				if (entry != null) {
					System.out.println("key : " + entry.getKey() + ", value : " + entry.getValue() + "====");
					final String paramKey = entry.getKey();
					final String paramValue = entry.getValue().toString();
					boundRequestBuilder.addFormParam(paramKey, paramValue);
				}
			}

		}

		try {
			Future<Response> responseFuture = boundRequestBuilder.execute();
			try {
				response = responseFuture.get();
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			} catch (ExecutionException e) {
				logger.error(e.getMessage(), e);
			}
		} catch (Exception e) {
			logger.error("Error in doing POST call for URL: " + url, e);
		}

		return response;

	}

	public Response doPostUrlResponseWithPostBody(String url, String postBody, Map<String, String> headers) throws IllegalArgumentException, UnsupportedEncodingException {

		Response response = null;

		BoundRequestBuilder boundRequestBuilder = asyncHttpClient.preparePost(url);

		if (headers != null && headers.size() > 0) {
			Set<Entry<String, String>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, String>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, String> entry = iterator.next();

				if (entry != null) {
					final String headerKey = entry.getKey();
					final String headerValue = entry.getValue();
					boundRequestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (postBody != null) {
			boundRequestBuilder.setBody(postBody.getBytes("UTF-8"));
		}

		try {
			Future<Response> responseFuture = boundRequestBuilder.execute();
			try {
				response = responseFuture.get();
			} catch (InterruptedException e) {
				logger.error("Error in doing POST call for URL: " + url, e);
			} catch (ExecutionException e) {
				logger.error("Error in doing POST call for URL: " + url, e);
			}
		} catch (Exception e) {
			logger.error("Error in doing POST call for URL: " + url, e);
		}

		return response;

	}

	public Response doPostUrlResponseWithPostBodyNew(String url, Object postBody, Map<String, Object> headers) throws IllegalArgumentException, UnsupportedEncodingException {

		Response response = null;

		BoundRequestBuilder boundRequestBuilder = asyncHttpClient.preparePost(url);

		if (headers != null && headers.size() > 0) {
			Set<Entry<String, Object>> entrySet = headers.entrySet();
			for (Iterator<Entry<String, Object>> iterator = entrySet.iterator(); iterator.hasNext();) {
				Entry<String, Object> entry = iterator.next();

				if (entry != null) {
					final String headerKey = entry.getKey();
					final String headerValue = entry.getValue().toString();
					boundRequestBuilder.addHeader(headerKey, headerValue);
				}
			}
		}

		if (postBody != null) {
			boundRequestBuilder.setBody(postBody.toString().getBytes("UTF-8"));
		}

		try {
			Future<Response> responseFuture = boundRequestBuilder.execute();
			try {
				response = responseFuture.get();
			} catch (InterruptedException e) {
				logger.error("Error in doing POST call for URL: " + url, e);
			} catch (ExecutionException e) {
				logger.error("Error in doing POST call for URL: " + url, e);
			}
		} catch (Exception e) {
			logger.error("Error in doing POST call for URL: " + url, e);
		}

		return response;

	}

	@Override
	protected void finalize() {
		try {

			asyncHttpClient.close();

		} catch (Exception e) {
			logger.error("Error closing ning pool", e);
		}
	}

}

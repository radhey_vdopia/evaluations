package com.vdopia.http.sync;
/**
 * Created by radheyshyam on 10/07/17.
 */
public class ClientConfigBuilder {

	private boolean compressionEnabled;
	private boolean followRedirects;
	private int connectionTimeoutInMs;
	private int idleConnectionInPoolTimeoutInMs;
	private int requestTimeoutInMs;
	private int maximumConnectionsPerHost;
	private int maximumConnectionsTotal;
	private int readTimeOut;

	public boolean isCompressionEnabled() {
		return compressionEnabled;
	}

	public void setCompressionEnabled(boolean compressionEnabled) {
		this.compressionEnabled = compressionEnabled;
	}

	public boolean isFollowRedirects() {
		return followRedirects;
	}

	public void setFollowRedirects(boolean followRedirects) {
		this.followRedirects = followRedirects;
	}


	public int getConnectionTimeoutInMs() {
		return connectionTimeoutInMs;
	}

	public void setConnectionTimeoutInMs(int connectionTimeoutInMs) {
		this.connectionTimeoutInMs = connectionTimeoutInMs;
	}

	public int getIdleConnectionInPoolTimeoutInMs() {
		return idleConnectionInPoolTimeoutInMs;
	}

	public void setIdleConnectionInPoolTimeoutInMs(int idleConnectionInPoolTimeoutInMs) {
		this.idleConnectionInPoolTimeoutInMs = idleConnectionInPoolTimeoutInMs;
	}

	public int getRequestTimeoutInMs() {
		return requestTimeoutInMs;
	}

	public void setRequestTimeoutInMs(int requestTimeoutInMs) {
		this.requestTimeoutInMs = requestTimeoutInMs;
	}

	public int getMaximumConnectionsPerHost() {
		return maximumConnectionsPerHost;
	}

	public void setMaximumConnectionsPerHost(int maximumConnectionsPerHost) {
		this.maximumConnectionsPerHost = maximumConnectionsPerHost;
	}

	public int getMaximumConnectionsTotal() {
		return maximumConnectionsTotal;
	}

	public void setMaximumConnectionsTotal(int maximumConnectionsTotal) {
		this.maximumConnectionsTotal = maximumConnectionsTotal;
	}

	public int getReadTimeOut() {
		return readTimeOut;
	}

	public void setReadTimeOut(int readTimeOut) {
		this.readTimeOut = readTimeOut;
	}
}

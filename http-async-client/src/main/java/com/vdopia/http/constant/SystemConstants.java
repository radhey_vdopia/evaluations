package com.vdopia.http.constant;

/**
 * Created by radheyshyam on 10/07/17.
 */
public class SystemConstants {
    public static final String ASYNC_CLIENT_DEFAULT = "defaultConfig";
    public static final String ASYNC_CLIENT_BIDDER_1 = "TestBidder1";
    public static final String ASYNC_CLIENT_BIDDER_2 = "TestBidder2";
    public static final String ASYNC_CLIENT_BIDDER_3 = "TestBidder3";
}

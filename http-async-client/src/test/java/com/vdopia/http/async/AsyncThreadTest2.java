package com.vdopia.http.async;

import com.vdopia.http.async.callback.HttpResponseCallbackHandlerIntf;
import com.vdopia.http.async.config.HttpConnectionCacheManager;
import com.vdopia.http.async.data.HttpAsyncResponse;
import com.vdopia.http.async.data.HttpRequest;
import com.vdopia.http.async.enums.HttpRequestType;
import com.vdopia.http.constant.SystemConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by radheyshyam on 10/07/17.
 */
public class AsyncThreadTest2 {
    public static void main(String[] args) {
        HttpConnectionCacheManager connectionCacheManager = new HttpConnectionCacheManager();
        try {
            connectionCacheManager.loadHttpAsyncConnections();
        } catch (Exception e) {
            e.printStackTrace();
        }
        AsyncThreadTest2 asyncThreadTest=new AsyncThreadTest2();
        asyncThreadTest.runThread();
    }

    public void runThread() {
        System.out.println("Thread Started -------------");
        final int CONCURRENCY_LEVEL = 30;
        long start = System.nanoTime();
        ExecutorService executor = Executors.newFixedThreadPool(CONCURRENCY_LEVEL);
        final HttpRequest httpRequest = new HttpRequest(SystemConstants.ASYNC_CLIENT_BIDDER_2, HttpRequestType.GET, "http://127.0.0.1:9090/testServer/getSlowest", null, null, null, new DummyHttpResponseCallbackHandlerImpl());

        for (int i = 0; i < 1000; i++) {
            System.out.println("Thread Number : " + i);
           /* Callable<Integer> worker = new Callable<Integer>() {

                public Integer call() throws Exception {
                    AsyncClientTest obj = new AsyncClientTest(httpRequest);
                    obj.run();
                    return 0;
                }
            };*/
            Future<?> submitted = executor.submit(new AsyncClientTest(httpRequest));
            if(i%CONCURRENCY_LEVEL==0)
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

        try {
            executor.shutdown();
        } finally {
            while (!executor.isTerminated()) {
            }
            long end = System.nanoTime();
            System.out.println("Terminated completely in " + (end - start) + "ns !!");
        }
    }

    private static class DummyHttpResponseCallbackHandlerImpl implements HttpResponseCallbackHandlerIntf {

        private static Logger logger = LoggerFactory.getLogger(DummyHttpResponseCallbackHandlerImpl.class);

        public void execute(HttpAsyncResponse httpAsyncResponse) {
            try {
                System.out.println(httpAsyncResponse.toString());
                logger.error(httpAsyncResponse.toString());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

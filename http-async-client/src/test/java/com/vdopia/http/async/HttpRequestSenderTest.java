package com.vdopia.http.async;

import com.vdopia.http.ChocolateAsyncClientTest;
import com.vdopia.http.async.callback.HttpResponseCallbackHandlerIntf;
import com.vdopia.http.async.config.HttpConnectionCacheManager;
import com.vdopia.http.async.data.HttpAsyncResponse;
import com.vdopia.http.async.data.HttpRequest;
import com.vdopia.http.async.enums.HttpRequestType;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by radheyshyam on 10/07/17.
 */
public class HttpRequestSenderTest {
    @Before
    public void setupPool()
    {
        HttpConnectionCacheManager connectionCacheManager = new HttpConnectionCacheManager();
        try {
            connectionCacheManager.loadHttpAsyncConnections();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
    public void call() throws Exception {
        HttpRequest httpRequest = new HttpRequest(null, HttpRequestType.GET, "http://www.rediff.com", null, null, null, new DummyHttpResponseCallbackHandlerImpl());
        HttpRequestSender httpRequestSender = new HttpRequestSender();
        httpRequestSender.call(httpRequest);

    }

    private static class DummyHttpResponseCallbackHandlerImpl implements HttpResponseCallbackHandlerIntf {

        private static Logger logger = LoggerFactory.getLogger(ChocolateAsyncClientTest.class);

        public void execute(HttpAsyncResponse httpAsyncResponse) {
            try {
                System.out.println(httpAsyncResponse.getResponse());
                logger.error(httpAsyncResponse.getResponse());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
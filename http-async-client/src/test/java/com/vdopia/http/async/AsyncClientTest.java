package com.vdopia.http.async;

import com.vdopia.http.async.callback.HttpResponseCallbackHandlerIntf;
import com.vdopia.http.async.config.HttpConnectionCacheManager;
import com.vdopia.http.async.data.HttpAsyncResponse;
import com.vdopia.http.async.data.HttpRequest;
import com.vdopia.http.async.enums.HttpRequestType;
import com.vdopia.http.constant.SystemConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by radheyshyam on 10/07/17.
 */
public class AsyncClientTest implements Runnable {


	private final HttpRequest httpRequest;

	public AsyncClientTest(HttpRequest httpRequest) {
		this.httpRequest = httpRequest;
	}

	public void run() {
		HttpRequestSender httpRequestSender = new HttpRequestSender();
		httpRequestSender.call(httpRequest);
	}
}
